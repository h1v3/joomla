package main

import (
	"fmt"
)

func php_str_noquotes(data string) string {
	//"Convert string to chr(xx).chr(xx) for use in php"
	var encoded string
	for char := range []byte(data) {
		encoded = encoded + fmt.Sprintf("chr(%s)", string(char))
	}
	return encoded
}
