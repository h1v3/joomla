package main

import (
	"fmt"
	"net/http"
)

func get_url(url, user_agent string) *http.Response {
	client := &http.Client{}
	req, err := http.NewRequest("GET", url, nil)
	checkErr(err)
	req.Header.Set("User-Agent", "Mozilla/5.0 (iPhone; CPU iPhone OS 5_0 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9A334 Safari/7534.48.3")
	req.Header.Set("X-Forwarded-For", user_agent)
	getCookies, err := client.Do(req)
	checkErr(err)
	if getCookies.Body != nil {
		cookies := getCookies.Cookies()
		defer getCookies.Body.Close()
		for _, cookie := range cookies {
			req.AddCookie(cookie)
		}
		var resp *http.Response
		for i := 1; i <= 5; i++ {
			resp, _ = client.Do(req)
		}
		checkErr(err)
		defer resp.Body.Close()
		return resp
	} else {
		fmt.Println("NPU")
	}
	return nil
}
